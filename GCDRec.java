public class GCDRec {
    public static void main(String[] args) {
        int num1= Integer.parseInt(args[0]);
        int num2 = Integer.parseInt(args[1]);

        System.out.println(gcd(num1,num2));
    }
    public static int gcd(int num1,int num2){
        if (num1 == 0){
            return num2;
        }
        return gcd(num2 % num1,num1);
    }
}
